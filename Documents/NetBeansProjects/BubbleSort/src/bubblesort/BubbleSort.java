/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bubblesort;
import java.util.Arrays;
/**
 *
 * @author ASUS
 */
public class BubbleSort {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
                System.out.println("~ Damar Raihan Choirul Firdaus / 15 / X RPL 6 ~"); 
                System.out.println(" ");
                // inisialisasi variable arrays yang bernama bilangan yang berisi 42,19, 1 dan 100
		int[] bilangan = {1, 2, 3, 4, 5, 6};  
		
		//mengeluarkan kalimat “ bilangan sebelum di sorting Bubble Sort” lalu ditambahkan Arrays.toString(bilangan) agar menampilkan perubahan dalam bentuk String.
		System.out.println("Bilangan sebelum di sorting Bubble Sort : "+ Arrays.toString(bilangan)); 
                
                
		//Proses Bubble Sort = mengeluarkan kalimat “Proses Bubble Sort secara Ascending” dengan \n = enter
		System.out.println("\nProses Bubble Sort secara Ascending:"); 
		for(int a = 0; a < bilangan.length; a++) {
                    
                //	Tampilkan proses Iterasi
			System.out.println("Iterasi "+(a+1));
			for(int b = 0; b < bilangan.length-1; b++) { if(bilangan[b] > bilangan[b+1]) {
                            
                //	proses pertukaran bilangan
					int temp = bilangan[b];
					bilangan[b] = bilangan[b+1];
					bilangan[b+1] = temp;
				}
				
                //	Tampilkan proses pertukaran tiap iterasi
				System.out.println(Arrays.toString(bilangan));
			}
                                System.out.println();
		}
		
		//	Tampilkan hasil akhir
		System.out.println("Hasil akhir setelah di sorting: "+Arrays.toString(bilangan));
	}
}

